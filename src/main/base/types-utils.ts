export interface ITyped<T> {
    [key: string]: T;
}

export interface IResponse {
    data: any;
}

export enum FieldTypeEnum {
    string = "text",
    number = "number",
    button = "button",
    checkBox ="checkbox",
    data = "date",
    email = "email",
    file = "file",
    password ="password",
    radio = "radio",
    submit = "submit",
    url = "url",
}