import { ITyped } from "./types-utils";

type ObjectEachHandler<T> = (cd: string, item: T, idx?: number) => void;
type TypedObjectHandler<T, V> = (cd: string, value: T) => V;


export function each<T>(object: ITyped<T>, fn: ObjectEachHandler<T>): void {
    if (object == null)
        return;

    let count = 0;
    for (const cd in object) {
        if (!object.hasOwnProperty(cd))
            continue;

        fn(cd, object[cd], count);
        count++;
    }
}

export function objectToArraySimple<T>(obj: ITyped<T>): T[] {
    const res = [];
    each(obj, (cd, value) => {
        if (value != null)
            res.push(value);
    });
    return res;
}

export function objectToArray<T, V>(obj: ITyped<T>, fn?: TypedObjectHandler<T, V>): V[] {
    if (!fn)
        return objectToArraySimple<V>(obj as ITyped<any>);

    const res = [];
    each(obj, (i, value) => {
        const r = fn(i, value);
        if (r != null)
            res.push(r);
    });
    return res;
}