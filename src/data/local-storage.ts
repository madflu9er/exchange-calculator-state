export const setItem = (key: string, value: any) => {
    try {
        localStorage.setItem(key, value);
    }
    catch(error){
        console.warn("Error occured while set item into LS: ", error);
    }
}

export const getItem = (key: string) => {
    try {
        const data = localStorage.getItem(key);
        return JSON.parse(data);
    }
    catch(error){
        console.warn("Error occured while get item from LS: ", error);
    }
}

export const clearStorage = () => {
    localStorage.clear();
}