const imgs = {
    BTC: "https://endotech.io/img/coinicon/BTC.png",
    ETH: "https://endotech.io/img/coinicon/ETH.png",
    XRP: "https://endotech.io/img/coinicon/XRP.png"
}

export const getImg = (name: string): string => imgs[name];