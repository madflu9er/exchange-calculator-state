import axios from "axios";
import URI from "./url";
import { IResponse } from "base/types-utils";
import { objectToArray } from "base/obj-extension";
import { setItem } from "data/local-storage";



export const getCurrencyData = (fsyms: Array<string>, tsyms: Array<string>, cb: (data: any) => void ) => {
    const url = URI.getMuliCourseURL(fsyms, tsyms);
    axios.get(url)
        .then((response: IResponse) => {
            const currencyArray = objectToArray(response.data, (cName, cData) => ({name: cName, data: cData}));
            const currency = JSON.stringify(currencyArray)

            setItem("coins-data", currency)
            if (cb)
                cb(currencyArray);

            return response.data;
        })
        .catch((error: any) => {
            console.error(error);
        });
}