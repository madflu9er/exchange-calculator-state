export interface ILabeledInput {
    label: string,
    value: number | string;
    readonly?: boolean;
    className?: string;
    type: string;
    onChange?: (d: number) => void;
}