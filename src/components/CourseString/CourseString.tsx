import React from "react";
import { ICourseString } from "./types";

const CourseString = ({name, value}: ICourseString) => {
    return (
        <div className="course-string">
            <span className="name">{name}:</span>
            <span className="value">{value}</span>
        </div>
    );
}

export default CourseString;