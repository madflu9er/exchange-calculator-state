export interface ICourseString {
    name: string;
    value: number;
}