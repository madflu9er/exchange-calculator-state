import React, {useEffect, useState} from "react";
import { getItem, clearStorage } from "data/local-storage";
import { getCurrencyData } from "api/currency";
import CoinBlock from "components/CoinBlock/CoinBlock";
import LabeledInput from "components/LabeledInput/LabeledInput";
import ExchangeOutput from "components/ExchangeOutput/ExchangeOutput";
import CurrencySelector from "components/CurrencySelector/CurrencySelector";
import { FieldTypeEnum } from "base/types-utils";

const ExchangeCalculator = () => {
    const [coinsData, setCoinsData] = useState(null);
    const [currentCoin, setCurrentCoin] = useState(null);
    const [currentCurrency, setCurrentCurrency] = useState(null);
    const [result, setResult] = useState(0);
    const [volume, setVolume] = useState(0);

    const fsyms = ["BTC","ETH","XRP"];
    const tsfms = ["USD","UAH","RUB"];

    const setDefaults = (data) => {
        setCoinsData(data);
        setCurrentCoin(data[0]);
        setCurrentCurrency(tsfms[0]);
    }

    const updateData = (data) => {
        setCoinsData(data);
        setCurrentCoin(data[0]);
        setCurrentCurrency(tsfms[0]);
    }

    useEffect(() => {
        if (coinsData && volume && currentCoin && currentCurrency) {
            const currencyValue = currentCoin.data[currentCurrency];
            const res = currencyValue * volume
            setResult(res);
        }

    }, [coinsData, volume, currentCoin, currentCurrency])

    useEffect(() => {
        const coinsData = getItem('coins-data');

        coinsData ? updateData(coinsData) : getCurrencyData(fsyms, tsfms, (data) => setDefaults(data));

        return () => {
            clearStorage();
        }
    }, []);

    return (
        <div className="container">
            <div className="coin-block-wrapper">
                {coinsData && coinsData.map(({name, data}) => (
                    <CoinBlock name={name} data={data} key={name} onSelect={setCurrentCoin}/>
                ))}
            </div>
            {currentCurrency &&
                <LabeledInput
                    label={"Selected coin"}
                    readonly
                    className={"output"}
                    value={currentCoin.name}
                    type={FieldTypeEnum.string}
                />
            }
            <LabeledInput
                label={"Volume"}
                onChange={setVolume}
                value={volume}
                type={FieldTypeEnum.number}
            />
            {currentCurrency &&
                <CurrencySelector
                    currencyList={tsfms}
                    onSelect={setCurrentCurrency}
                    current={currentCurrency}
                />
            }
            {currentCoin &&
                <ExchangeOutput
                    volume={volume}
                    coinName={currentCoin.name}
                    currencyName={currentCurrency}
                    amount={result}
                />
            }
        </div>
    )
}

export default ExchangeCalculator;