export interface IExchangeOutput {
    volume: number;
    coinName: string;
    amount: number;
    currencyName: string;
}