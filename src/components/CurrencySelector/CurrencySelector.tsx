import React from "react";
import { ICurrencySelector } from "./types";


const CurrencySelector = ({currencyList, onSelect, current}: ICurrencySelector) => {

    return (
        <div className="currency-selector">
            {currencyList && currencyList.map(currency => (
                <input
                    className={`${currency === current ? "active" : ""}`}
                    type="submit"
                    value={currency}
                    key={currency}
                    onClick={() => onSelect(currency)}
                />
            ))}
        </div>
    )
}

export default CurrencySelector;