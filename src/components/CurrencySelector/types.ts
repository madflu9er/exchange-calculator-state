export interface ICurrencySelector {
    currencyList: string[];
    current: string;
    onSelect: (d: string) => void;
}