import { ITyped } from "base/types-utils";

export interface ICoin {
    name: string;
    data: ITyped<number>;
    onSelect: (a: any) => void;
}