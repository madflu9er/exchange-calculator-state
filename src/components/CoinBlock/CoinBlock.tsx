import React from "react";
import { ICoin } from "./types";
import { getImg } from "data/currency-img";
import { objectToArray } from "base/obj-extension";
import CourseString from "components/CourseString/CourseString";


const CoinBlock = ({ name, data, onSelect }: ICoin) => {

    const imgSrc = getImg(name);
    const dataAsArray = objectToArray(data, (cName, value) => ({name:cName, value}));

    return (
        <div className="coin-block" onClick={() => onSelect({name, data})}>
            <div className="coin-img">
                <img src={imgSrc} alt={name} />
                <div className="name">{name}</div>
            </div>
            <div className="coin-data">
                {dataAsArray && dataAsArray.map(({name, value}) => (
                    <CourseString name={name} value={value} key={name+value}/>
                ))}
            </div>
        </div>
    )
}

export default CoinBlock;